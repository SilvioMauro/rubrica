import React from 'react';
import {FaBomb} from 'react-icons/fa';


const Card = (props) => {
    return (
      <>
        <ul className="user-list">
          {props.data.map((person) => (
            <li key={person.id}>
              {" "}
              <Person {...person} removeItem={props.removeItem} />
            </li>
          ))}
        </ul>
      </>
    );
  };


  

  
   
  const Person = ({ id, name, surname, phrase, img, removeItem }) => {
    return (
      <article>
        <img src={img} alt="prs" className="person-img" />
        <div className="person-info">
          <div className="person-action">
            <h4>{name} {surname}</h4>
            <button className="btn" onClick={() => removeItem(id)}>
              {" "}
              <FaBomb className="icon" />{" "}
            </button>
          </div>
          <p>'{phrase}'</p>
        </div>
      </article>
    );
  };
       
          



export default Card;